// =======================================================================================
// Synchronus Thunk:
// =======================================================================================

function add(x, y) {
  return x + y;
}

var thunk = function() {
  return add(10, 15);
};

thunk(); // 25


// =======================================================================================
// Asynchronus Thunk:
// =======================================================================================
function addAsync(x, y, cb) {
  setTimeout(function() {
    cb( x + y );
  }, 1000);
}

var asyncThunk = function(cb) {
  return add(10, 15, cb);
};

asyncThunk(function(sum){
  sum; // 25
});


// =======================================================================================
// **Kind of like** a Promise Constructor:
// =======================================================================================
function makeThunk(fn) {
  var args = [].slice.call(arguments,1);
  return function(cb) {
    args.push(cb);
    fn.apply(null, args);
  };
}

function addAsync(x, y, cb) {
  setTimeout(function() {
    cb( x + y );
  }, 1000);
}

var thunk = makeThunk(addAsync, 10, 15);

thunk(function(sum){
  console.log(sum); // 25
});
