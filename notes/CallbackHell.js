// =======================================================================================
// Callbacks or "The Pyramid of Doom"
// =======================================================================================
setTimeout(() => {
  console.log("Callback!");
}, 1000);

// =======================================================================================
// "Callback Hell"
// =======================================================================================
setTimeout(function() {
  console.log("One");
  setTimeout(function() {
    console.log("Two");
    setTimeout(function() {
      console.log("Three");
    }, 1000);
  }, 1000);
}, 1000);

// =======================================================================================
// Same Hellscape
// =======================================================================================
function One(cb) {
  console.log("One");
  setTimeout(cb, 1000);
}
function Two(cb) {
  console.log("Two");
  setTimeout(cb, 1000);
}
function Three() {
  console.log("Three");
}
One(function(){
  two(three());
});

