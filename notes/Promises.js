function trackCheckout(info){
  return new Promise(
    function(resolve, reject){
      // attempt to track the checkout

      // if successful, call resolve()
      // otherwise, call reject(error)
    }
  )
}

function finish(){
  chargeCreditCard(purchaseInfo);
  showThankYouPage();
}

function error(err) {
  logStatsError(err);
  finish();
}

var promise = trackCheckout(purchaseInfo);

promise.then(
  finish,
  error
)


// =======================================================================================
// Promise Flow Control:
// =======================================================================================
function delay(num) {
  return new Promise(function(resolve, reject){
    setTimeout(resolve, num);
  })
}

delay(100)
.then(function(){
  return delay(50);
})
.then(function(){
  return delay(200);
})
.then(function(){
  console.log("all done!");
});


// =======================================================================================
// Promise "gate"
// =======================================================================================
Promise.all([
  doTask1a(),
  doTask1b(),
  doTask1c()
])
.then(function(results){
  return doTask2(
    Math.max(
      results[0],
      results[1],
      results[2]
    )
  )
})

// =======================================================================================
// Promise Timeouts
// =======================================================================================

var p = trySomeAsyncThing();

Promise.race([
  p,
  new Promise(function(_, reject){
    setTimeout(() => {
      reject("Timeout!");
    }, 3000);
  })
])
.then(
  success,
  error
)
