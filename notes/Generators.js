function* gen() {
  console.log('Hello');
  yield;
  console.log('World');
}

var it = gen();
it.next();  // Hello
it.next();  // World

function *main() {
  yield 1;
  yield 2;
  yield 3;
}

// =======================================================================================
// Messages
// =======================================================================================
var it = main();

it.next(); // { value: 1, done: false }
it.next(); // { value: 2, done: false }
it.next(); // { value: 3, done: false }

it.next(); // { value: undefined, done: true }

// =======================================================================================
// Coroutines
// =======================================================================================
function coroutine(g) {
  let it = g();
  return function(){
    return it.next().apply(it, arguments);
  };
}

var step = coroutine(function*(){
  var x = 1 + (yield);
  var y = 1 + (yield);
  yield ( x + y );
});

step();
step(10);
console.log(
  `Meaning of life: ${step(30).value}.`
);

function getData(d) {
  setTimeout(function(){ step(d); }, 1000);
}

var run = coroutine(function*(){
  var x = 1 + (yield getData(10));
  var y = 1 + (yield getData(30));
  var answer = (yield getData(
    `Meaning of life: ${ x + y }`
  ));
  console.log(answer);
  // Meaning of life: 42
});

run();
