/**
 *   - Request all 3 files at the sime time (in "parallel").
 *   - Render them ASPA (don't just blindly wait for all to finish loading)
 *   - BUT, render them in proper (obvious) order: "file1", "file2", "file3".
 *   - After all 3 are done, output "Complete!".
 */

let fakeAJAX = require('./util').fakeAJAX;
let output = require('./util').output;


function getFile(file) {
  return new Promise(function(res) {
    fakeAJAX(file,res);
  });
}

// Request all files at once in "parallel"
var p1 = getFile('file1');
var p2 = getFile('file2');
var p3 = getFile('file3');

p1
  .then(output)
  .then(function(){
    return p2;
  })
  .then(output)
  .then(function(){
    return p3;
  })
  .then(output)
  .then(function(){
    output('Complete!');
  })
  .catch(function(err){
    return err;
  });

