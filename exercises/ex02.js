/**
 *   - Request all 3 files at the sime time (in "parallel").
 *   - Render them ASPA (don't just blindly wait for all to finish loading)
 *   - BUT, render them in proper (obvious) order: "file1", "file2", "file3".
 *   - After all 3 are done, output "Complete!".
 */

let fakeAJAX = require('./util').fakeAJAX;
let output = require('./util').output;

function getFile(file) {
  var text, fn;

  fakeAJAX(file, function(response) {
    if (fn) fn(response);
    else text = response;
  });

  return function(cb){
    if (text) cb(text);
    else fn = cb;
  };
}

// Request all files at once in "parallel"
var th1 = getFile('file1');
var th2 = getFile('file2');
var th3 = getFile('file3');

th1(function(text1){
  output(text1);
  th2(function(text2){
    output(text2);
    th3(function(text3){
      output(text3);
      output('Complete!');
    });
  });
});
