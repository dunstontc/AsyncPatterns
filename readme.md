# Rethinking Asynchronous Programming by Kyle Simpson


## Async Patterns
  - Parallel vs Async
  - Callbacks
  - Thunks
  - Promises
  - Generators/Coroutines
  - Observables
  - CSP (Communicating Sequential Processes), channel-oriented concurrency


## Callbacks
  - Two Problems with callbacks:
    - Inversion of Control
    - Not **Reason**able

### Inversion of Control
  - Trust:
    - not too early
    - not too late
    - not too many times
    - not too few times
    - no lost context
    - no swallowed errors

### Not Reasonable
  Temporal Dependency - When one thing depends upon another thing finishing before it can go.

### Non Fixes
  - separate callbacks
  - error-first style


## Thunks
  - A function with closured state keeping track of a value or values that will return said values whenever it is called.
  - A function that doesn't need any arguments to execute.
  - Container wrapping a particular section of state.
  - **Using closure to maintain state.**

### Asynchronous Thunk
  - A function that doesn't need any arguments passed to it to do it's job, except you need to pass it a callback to get the value.
  - Time-independent wrapper around a value.
  - Don't solve inversion of control.
  

## Promises
  - IOU
  - A value that will at some unspecified time be fulfilled, but until then we need to be able to reason about it.
  - A pattern for managing our callbacks in a trustable fashion.

### Promise Trust
  1. only resolved once
  2. either success OR error
  3. messages passed/kept
  4. exceptions become errors
  5. **immutable** once resolved

### Flow Control
  - Chaining Promises
  - The way you take promise A, and chain it with promise B is that in the promise A, when you set up a then handling for promise A, in that handler you return promise B.
  - You return 1 promise; You return the second promise from the success handler of the first promise.


## Generators
- *State Machine* - Patterned series of flow from one state to more and declaritively listing those states.
- A function that can pause and resume as many times as necessary.
- Executing a generator produces an iterator.


## Misc.
  - Ands not Ors.
  - There is no one silver bullet.
  - *Action at a distance* - when you design a system where somebody else far away can do something that affects you without you knowing it. Bad idea.
    - Cancelable Promises != bueno
  - The point at which code diverges from the way our minds work is when bugs occur.
  - [kinda notes](https://github.com/getify/A-Tale-Of-Three-Lists)
  - Inversion of control (*the path to the dark side*) is what separates a library from a framework.
  - *Thenable* - Acts like a promise; not really a promise.
  - *map()* - Takes a list of values and transforms it.
  - *reduce()* - Takes a list of values and compose them together.
  - All functions in JavaScript that have no return type have an implicit *undefined* return
